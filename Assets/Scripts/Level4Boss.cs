using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level4Boss : MonoBehaviour
{
    private static Level4Boss _instance;
    public static Level4Boss instance {get {return _instance;} }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}

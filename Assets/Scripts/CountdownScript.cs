using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class CountdownScript : MonoBehaviour
{
    public float countdownTimer;
    public TextMeshProUGUI countdownText;
    public UnityEvent onFinishCountdown;
    Player player;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CountdownRush());
        player = GameObject.Find("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator CountdownRush()
    {
        while(countdownTimer > 0)
        {
            yield return new WaitForSeconds(1f);
            countdownTimer--;
            countdownText.text = "" + countdownTimer.ToString();
        }

        if(Level4Boss.instance)
            onFinishCountdown.Invoke();
        player.OnDie();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class OnEnableScene : MonoBehaviour
{
    public UnityEvent OnThisEnable;

    private void OnEnable()
    {
        OnThisEnable.Invoke();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class BossHealthPart : MonoBehaviour
{
    public UnityEvent OnDeath;
    public float Health;
    float totalHealth;
    bool isDead = false;

    [SerializeField] Material hitMaterial;
    Material defaultMaterial;
    SpriteRenderer spriteRenderer;
    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        defaultMaterial = spriteRenderer.material;
        totalHealth = Health;
    }

    public void TakeDamage(float num)
    {
        Health -= num;

        spriteRenderer.material = hitMaterial;

        if (Health <= 0)
        {
            if(!isDead)
            {
                isDead = true;
                OnDeath.Invoke();
                Destroy(transform.parent.gameObject);
            }
        }

        Invoke("ResetMaterial", 0.1f);
    }

    private void ResetMaterial()
    {
        spriteRenderer.material = defaultMaterial;
    }


    public float GetHealth()
    {
        return totalHealth;
    }

}

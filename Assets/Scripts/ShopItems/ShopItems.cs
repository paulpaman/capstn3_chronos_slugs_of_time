using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShopItems : MonoBehaviour
{
    public bool isUnlimited;
    public int Cost;
    public int CostIncreasePerLevel;
    public int MaxLevel;
    protected int currentLevel = 0;
    public TextMeshProUGUI CostText;
    public TextMeshProUGUI CoinText;
    protected Player player;

    protected virtual void OnEnable()
    {
        if(player)
        {
            CoinText.text = player.GetCoin().ToString();
        }
        
    }

    protected virtual void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        CostText.text = "Cost: " + Cost.ToString();
        CoinText.text = player.GetCoin().ToString();
    }

    public virtual void ApplyItem()
    {

    }

    protected virtual void IncreaseLevel()
    {
        currentLevel++;
        Cost += CostIncreasePerLevel;
        if(currentLevel < MaxLevel)
        {
            CostText.text = "Cost: " + Cost.ToString();
        }
        else
        {
            CostText.text = "Upgrade Maxed";
        }   
    }
}

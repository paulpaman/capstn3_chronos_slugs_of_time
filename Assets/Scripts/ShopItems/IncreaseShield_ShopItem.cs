using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IncreaseShield_ShopItem : ShopItems
{
    public float AddToMaxArmor;
    public TextMeshProUGUI LevelIncreaseText;
    float curArmor;
    float nextArmor;

    protected override void OnEnable()
    {
        base.OnEnable();
        if(player) SetArmorText();
    }

    protected override void Start()
    {
        base.Start();
        SetArmorText();
    }

    public override void ApplyItem()
    {
        base.ApplyItem();
        if (!isUnlimited)
        {
            if (currentLevel >= MaxLevel) return;
        }

        if (player.GetCoin() >= Cost)
        {
            if (PlayerStatsManager.Instance)
            {
                //player.SubtractCoin(Cost);
                PlayerStatsManager.Instance.SubtractPlayerPowerCells(Cost);
            }

            CoinText.text = player.GetCoin().ToString();

            //player.ChangeMaxArmor(AddToMaxArmor);
            if(PlayerStatsManager.Instance)
            {
                PlayerStatsManager.Instance.AddPlayerMaxArmor(AddToMaxArmor);
            }
            SetArmorText();
            player.UpdateScreenCrack();

            if (!isUnlimited)
            {
                if (currentLevel < MaxLevel)
                {
                    IncreaseLevel();
                    if (currentLevel >= MaxLevel)
                    {
                        LevelIncreaseText.text = "Shield Maxed";
                    }
                }
            }
        }    
    }

    void SetArmorText()
    {
        curArmor = player.GetMaxArmor();
        nextArmor = curArmor + AddToMaxArmor;
        LevelIncreaseText.text = curArmor.ToString() + " -> " + nextArmor;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoReplenish_ShopItems : ShopItems
{
    public WeaponController weaponController;

    public int AmmoToAdd;
    protected override void Start()
    {
        base.Start();
    }

    public override void ApplyItem( )
    {
        base.ApplyItem();
        if (!isUnlimited)
        {
            if (currentLevel >= MaxLevel) return;
        }

        if(weaponController.TotalAmmo >= weaponController.MaxAmmo)
        {
            return;
        }

        if (player.GetCoin() >= Cost)
        {
            if (PlayerStatsManager.Instance)
            {
                //player.SubtractCoin(Cost);
                PlayerStatsManager.Instance.SubtractPlayerPowerCells(Cost);
            }

            CoinText.text = player.GetCoin().ToString();
            weaponController.AddTotalAmmo(AmmoToAdd);
            if (!isUnlimited)
            {
                if (currentLevel < MaxLevel) IncreaseLevel();
            }
        }    
    }
}

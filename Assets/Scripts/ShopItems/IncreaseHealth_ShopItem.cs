using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IncreaseHealth_ShopItem : ShopItems
{
    public float AddToMaxHealth;
    public TextMeshProUGUI LevelIncreaseText;
    float curHealth;
    float nextHealth;

    protected override void OnEnable()
    {
        base.OnEnable();
        if(player) SetHealthText();
    }

    protected override void Start()
    {
        base.Start();
        SetHealthText();
    }

    public override void ApplyItem()
    {
        base.ApplyItem();
        if (!isUnlimited)
        {
            if (currentLevel >= MaxLevel) return;
        }

        if(player.GetCoin() >= Cost)
        {
            if (PlayerStatsManager.Instance)
            {
                //player.SubtractCoin(Cost);
                PlayerStatsManager.Instance.SubtractPlayerPowerCells(Cost);
            }

            CoinText.text = player.GetCoin().ToString();

            //player.ChangeMaxHealth(AddToMaxHealth);
            if (PlayerStatsManager.Instance)
            {
                PlayerStatsManager.Instance.AddPlayerMaxHealth(AddToMaxHealth);
            }
            SetHealthText();

            if (!isUnlimited)
            {
                if (currentLevel < MaxLevel)
                {
                    IncreaseLevel();
                    if (currentLevel >= MaxLevel)
                    {
                        LevelIncreaseText.text = "Health Maxed";
                    }
                }
            }
        }
      
    }

    void SetHealthText()
    {
        curHealth = player.GetMaxHealth();
        nextHealth = curHealth + AddToMaxHealth;
        LevelIncreaseText.text = curHealth.ToString() + " -> " + nextHealth;
    }
}

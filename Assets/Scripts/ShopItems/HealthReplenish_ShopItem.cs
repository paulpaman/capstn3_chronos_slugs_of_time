using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthReplenish_ShopItem : ShopItems
{
    protected override void Start()
    {
        base.Start();
    }

    public override void ApplyItem()
    {
        base.ApplyItem();
        if (!isUnlimited)
        {
            if (currentLevel >= MaxLevel) return;
        }

        if(player.GetCurrentHealth() >= player.GetMaxHealth())
        {
            return;
        }

        if (player.GetCoin() >= Cost)
        {
            if(PlayerStatsManager.Instance)
            {
                //player.SubtractCoin(Cost);
                PlayerStatsManager.Instance.SubtractPlayerPowerCells(Cost);
            }

           
            CoinText.text = player.GetCoin().ToString();
            player.ReplenishHealth();
            if (!isUnlimited)
            {
                if (currentLevel < MaxLevel) IncreaseLevel();
            }
        }       
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    private void Awake()
    {
        
    }

    private void Start()
    {
        StartCoroutine(DeletePersistentObjects());
    }

    IEnumerator DeletePersistentObjects()
    {
        yield return new WaitForSeconds(0.5f);
        if (GameObject.Find("PlayerStatsManager"))
        {
            Destroy(GameObject.Find("PlayerStatsManager"));
        }

        if (GameObject.Find("ProgressSceneLoaders"))
        {
            Destroy(GameObject.Find("ProgressSceneLoaders"));
        }
    }
}

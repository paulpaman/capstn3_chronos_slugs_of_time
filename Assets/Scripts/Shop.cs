using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public GameObject ShopPanel;
    bool canOpenShop = false;
    void Start()
    {
        ShopPanel.SetActive(false);
    }

    private void Update()
    {
        if(canOpenShop)
        {
            if(Input.GetKeyDown(KeyCode.F))
            {
                if (ShopPanel.activeSelf)
                {
                    ShopPanel.SetActive(false);
                    Time.timeScale = 1;
                    Cursor.lockState = CursorLockMode.Confined;
                    Cursor.visible = false;
                    //InteractText.Instance.ToggleText();
                }
                else
                {
                    ShopPanel.SetActive(true);
                    //InteractText.Instance.ToggleText();
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    Time.timeScale = 0;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            canOpenShop = true;
            if (InteractText.Instance)
            {
                InteractText.Instance.Text.text = "[F] Interact";
                InteractText.Instance.ToggleText();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            canOpenShop = false;
            if (InteractText.Instance) InteractText.Instance.ToggleText();
        }
    }

    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    HealthComponent health;
    ArmorComponent armor;
    public GameObject hitScreen;
    public Image armorUI;
    public Image healthUI;
    public Image ScreenCrackUI;
    public GameObject damageEffect;
    public GameObject glowEffect;
    public GameObject deathPanel;
    bool isTakingDamage = false;
    bool canPlayCrackSound = true;
    public GameObject BulletTarget;
    int coins;

    float flameDamage = 0;
    int numOfFlameTicks = 0;
    float flameTickInterval = 0;
    IEnumerator coroutine;

    void Start()
    {
        //Time.timeScale = 1;
        rb = GetComponent<Rigidbody2D>();
        health = GetComponent<HealthComponent>();
        armor = GetComponent<ArmorComponent>();

        if (PlayerStatsManager.Instance) PlayerStatsManager.Instance.SetPlayerStats();

        UIManager.Instance.UpdateHealthText((int)health.GetCurrentHealth());
        damageEffect.SetActive(false);
        ScreenCrackUI.fillAmount = 0;
    }


    void Update()
    {
        if (Time.timeScale == 0)
        {
            return;
        }

        if (armor.GetCurrentArmor() != armor.MaxArmor && !armor.isRegen)
        {
            armor.StartArmorRegen();
        }

        // Hit Effect
        if (isTakingDamage)
        {
            StartCoroutine("PlayDamageEffect");
        }
    }

    public void TakeDamage(float amount)
    {
        if (armor.isRegen)
        {
            armor.StopArmorRegen();
        }

        // Hit Screen effect
        // var color = hitScreen.GetComponent<Image>().color;
        // color.a = 0.5f;

        // hitScreen.GetComponent<Image>().color = color;
        isTakingDamage = true;


        //Save the amount of damage remaining off.
        float damageLeft = amount;

        // Should armor damage be calculated?
        if (armor.GetCurrentArmor() > 0)
        {
            int armorDamage = (int)Mathf.Min(damageLeft, armor.GetCurrentArmor());
            armor.ReduceArmor(armorDamage);
            damageLeft -= armorDamage;
            armorUI.fillAmount = armor.GetCurrentArmor() / armor.MaxArmor;
            ScreenCrackUI.fillAmount = CalculateScreenCrack();
        }

        if (damageLeft >= 0)
        {
            health.ReduceHealth(damageLeft);
            healthUI.fillAmount = health.GetCurrentHealth() / health.MaxHealth;
        }

        if(armor.GetCurrentArmor() <= 0)
        {
            StartCoroutine(CrackSoundCooldown());
        }

        if (health.GetCurrentHealth() <= 0)
        {
            OnDie();
        }
    }

    public void UpdateScreenCrack()
    {
        ScreenCrackUI.fillAmount = CalculateScreenCrack();
    }    

    public void AddHealth(int num)
    {
        health.AddHealth(num);
        healthUI.fillAmount = health.GetCurrentHealth() / health.MaxHealth;
    }

    public float CalculateScreenCrack()
    {
        return Mathf.InverseLerp(armor.MaxArmor, 0, armor.GetCurrentArmor());
    }

    void EnableDamageEffect()
    {
        glowEffect.SetActive(false);
        damageEffect.SetActive(true);
    }

    void DisableDamageEffect()
    {
        damageEffect.SetActive(false);
        glowEffect.SetActive(true);
    }

    IEnumerator PlayDamageEffect()
    {
        EnableDamageEffect();
        yield return new WaitForSeconds(0.1f);
        DisableDamageEffect();
        isTakingDamage = false;
    }

    IEnumerator ApplyFlameDamage()
    {
        for(int i = 0; i < numOfFlameTicks; i++)
        {
            TakeDamage(flameDamage);
            yield return new WaitForSeconds(flameTickInterval);
        }
    }

    IEnumerator CrackSoundCooldown()
    {
        if(canPlayCrackSound)
        {
            canPlayCrackSound = false;
            FindObjectOfType<AudioManager>().Play("armorCrack");
        }

        yield return new WaitForSeconds(3f);

        canPlayCrackSound = true;
    }
    public void ChangeMaxHealth(float num)
    {
        //float newHealth = health.GetCurrentHealth() + num;
        health.SetMaxHealth(num);
        healthUI.fillAmount = health.GetCurrentHealth() / health.MaxHealth;
    }

    public void ChangeMaxArmor(float num)
    {
        //float newArmor = armor.GetCurrentArmor() + num;
        armor.SetMaxArmor(num);
        armorUI.fillAmount = armor.GetCurrentArmor() / armor.MaxArmor;
    }

    public void ReplenishHealth()
    {
        health.AddHealth(health.MaxHealth);
        healthUI.fillAmount = health.GetCurrentHealth() / health.MaxHealth;
    }

    public void ReplenishArmor()
    {
        armor.AddArmor(armor.MaxArmor);
        armorUI.fillAmount = armor.GetCurrentArmor() / armor.MaxArmor;
    }

    public void EnableFlameDamage(float damage, int numOfTicks, float tickInterval)
    {
        flameDamage = damage;
        numOfFlameTicks = numOfTicks;
        flameTickInterval = tickInterval;
        coroutine = ApplyFlameDamage();
        StartCoroutine(coroutine);
    }

    public void DisableFlameDamage()
    {
        StopCoroutine(coroutine);
    }

    public void AddCoin(int num)
    {
        coins += num;
    }

    public void SubtractCoin(int num)
    {
        coins -= num;
    }

    public void SetCoin(int num)
    {
        coins = num;
    }

    public int GetCoin()
    {
        return coins;
    }

    public float GetCurrentHealth()
    {
        float curHealth = health.GetCurrentHealth();
        return curHealth;
    }

    public float GetCurrentArmor()
    {
        float curArmor = armor.GetCurrentArmor();
        return curArmor;
    }

    public float GetMaxArmor()
    {
        float curMaxArmor = armor.MaxArmor;
        return curMaxArmor;
    }

    public float GetMaxHealth()
    {
        float curMaxHealth = health.MaxHealth;
        return curMaxHealth;
    }

    public void OnDie()
    {
        Time.timeScale = 0;
        deathPanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

}

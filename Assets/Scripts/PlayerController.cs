using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb;
    public float MoveSpeed = 5f;
    public float MouseSensitivity = 1f;

    public Camera ViewCam;

    public Animator Anim;
    public GameObject pausePanel;

    Vector2 MoveInput;
    Vector2 MouseInput;

    Vector3 moveHorizontal;
    Vector3 moveVertical;
    public GameObject hudEffect;
    public RawImage MapImage;

    private void Start()
    {
        MapImage.enabled = false;
    }
    private void Update()
    {
        
        if(Time.timeScale == 0)
        {
            return;
        }


        MoveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        moveHorizontal = transform.up * -MoveInput.x;
        moveVertical = transform.right * MoveInput.y;

        MouseInput = new Vector2(Input.GetAxisRaw("Mouse X"),0) * MouseSensitivity; //Input.GetAxisRaw("Mouse Y")) * MouseSensitivity;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z - MouseInput.x);

        ViewCam.transform.localRotation = Quaternion.Euler(ViewCam.transform.localRotation.eulerAngles + new Vector3(0f, MouseInput.y, 0f));


        //HeadBob
        if (MoveInput != Vector2.zero)
        {
            Anim.SetBool("isMoving", true);
        }
        else
        {
            Anim.SetBool("isMoving", false);
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0;
            pausePanel.SetActive(true);
            hudEffect.SetActive(false);
        }

        //Map
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            MapImage.enabled = !MapImage.isActiveAndEnabled;
        }

    }

    private void FixedUpdate()
    {
        rb.velocity = (moveHorizontal + moveVertical) * MoveSpeed;
    }
    
}

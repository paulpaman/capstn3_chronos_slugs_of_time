using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCrate : MonoBehaviour
{
    HealthComponent health;
    [SerializeField] Material hitMaterial;
    SpriteRenderer spriteRenderer;
    Material defaultMaterial;
    bool isDead = false;
    // Start is called before the first frame update
    void Start()
    {
        health = GetComponent<HealthComponent>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        defaultMaterial = spriteRenderer.material;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(float damage)
    {
        spriteRenderer.material = hitMaterial;
        health.ReduceHealth(damage);
        if(health.GetCurrentHealth() <= 0)
        {
            if(!isDead)
            {
                isDead = true;
                if(Level2Objectives.instance)
                {
                    Level2Objectives.instance.AddDestroyedCrates();
                }
                if(Level3Objectives.instance)
                {
                    Level3Objectives.instance.AddDestroyedCrates();
                }
                Destroy(transform.parent.gameObject);
            }
        }

        Invoke("ResetMaterial", 0.1f);
    }

    private void ResetMaterial()
    {
        spriteRenderer.material = defaultMaterial;
    }
}

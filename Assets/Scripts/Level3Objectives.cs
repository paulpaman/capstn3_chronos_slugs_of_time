using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level3Objectives : MonoBehaviour
{
    private static Level3Objectives _instance;
    public static Level3Objectives instance {get {return _instance;} }
    public int destroyedCrates = 0;
    public ObjectiveChecker objective1;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if(destroyedCrates >= 9)
        {
            objective1.StrikeThroughText();
            WeaponController.Instance.UnlockMinigun();
        }
    }

    public void AddDestroyedCrates()
    {
        destroyedCrates++;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawning : MonoBehaviour
{
    [SerializeField] WaveData[] waveDatas;

    public Transform SpawnPos;
    int waveIndex;
    int totalEnemies;

    public bool isUnlimitedSpawning;
    public float WaveSpawnTime;
    public float EnemySpawnTime;

    public float WaveSpawnInteral;

    IEnumerator coroutine;
    private void Start()
    {
        if(!isUnlimitedSpawning)
        {
            waveIndex = 0;
            totalEnemies = waveDatas[waveIndex].TotalWave;
        }

        Spawn();
    }
    void SpawnEnemy(Enemy enemyToSpawn)
    {
        Enemy enemy = Instantiate(enemyToSpawn, SpawnPos.position, Quaternion.identity);

        if(!isUnlimitedSpawning)
        {
            //enemy.OnDeath.AddListener(OnEnemyKill);
        }
        
    }

    IEnumerator SpawnWaveUnlimited()
    {
        while(true)
        {
            for (int i = 0; i < waveDatas[waveIndex].EnemyList.Length; i++)
            {
                SpawnEnemy(waveDatas[waveIndex].EnemyList[i]);

                yield return new WaitForSeconds(EnemySpawnTime);
            }

            yield return new WaitForSeconds(WaveSpawnInteral);
        }
      
    }

    IEnumerator SpawnWaveLimited()
    {
        yield return new WaitForSeconds(WaveSpawnTime);

        for (int i = 0; i < waveDatas[waveIndex].EnemyList.Length; i++)
        {
            SpawnEnemy(waveDatas[waveIndex].EnemyList[i]);

            yield return new WaitForSeconds(EnemySpawnTime);
        }
    }

    void Spawn()
    {
        if(isUnlimitedSpawning)
        {
            coroutine = SpawnWaveUnlimited(); 
        }
        else
        {
            coroutine = SpawnWaveLimited();
        }

        StartCoroutine(coroutine);
    }

}

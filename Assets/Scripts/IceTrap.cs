using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceTrap : MonoBehaviour
{
    public Animator animator;
    public GameObject IceDebuff;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            BuffReceiver buffReceiver = collision.GetComponent<BuffReceiver>();
            if (buffReceiver)
            {
                BuffAPI armorDebuff = Instantiate(IceDebuff.GetComponent<BuffAPI>());
                buffReceiver.ApplyBuff(armorDebuff);
            }
        }
    }

    public void TriggerTrap()
    {
        animator.SetTrigger("Trap");
    }
}

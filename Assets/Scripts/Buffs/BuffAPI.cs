using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffAPI : MonoBehaviour
{
    public float Duration;
    public BuffReceiver Target { get; private set; }

    protected virtual void OnActivate(BuffReceiver target) { }
    protected virtual void OnDeactivate(BuffReceiver target) { }

    public void setDuration(float durationTime)
    {
        Duration = durationTime;
    }

    private void Update()
    {
        Duration -= Time.deltaTime;
        if (Duration <= 0)
        {
            if (Target != null) Target.GetComponent<BuffReceiver>().RemoveBuff(this);
        }
        if (Target == null)
        {
            Destroy(this.gameObject);
        }
    }

    public void Replace()
    {
        if (Target != null) Target.GetComponent<BuffReceiver>().ReplaceBuff(this);
    }

    public void Activate(BuffReceiver target)
    {
        Target = target;
        OnActivate(Target);
    }

    public void Deactivate(BuffReceiver target)
    {
        OnDeactivate(Target);
        Destroy(this.gameObject);
    }
}

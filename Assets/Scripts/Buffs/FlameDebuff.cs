using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameDebuff : BuffAPI
{
    public float FlameDamage;
    public int NumberOfTicks;
    public float IntervalBetweenTicks;

    protected override void OnActivate(BuffReceiver target)
    {
        //Debuff Here
        target.GetComponent<Player>().EnableFlameDamage(FlameDamage,NumberOfTicks,IntervalBetweenTicks);
    }
    protected override void OnDeactivate(BuffReceiver target)
    {
        //RemoveDebuff Here
        target.GetComponent<Player>().DisableFlameDamage();
    }
}


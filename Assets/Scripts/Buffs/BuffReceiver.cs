using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffReceiver : MonoBehaviour
{
    public List<BuffAPI> buffs = new List<BuffAPI>();

    public void ApplyBuff(BuffAPI buff)
    {
        //checkBuff(buff);
        buffs.Add(buff);
        buff.Activate(this);
    }

    public void RemoveBuff(BuffAPI buff)
    {
        buffs.Remove(buff);
        buff.Deactivate(this);
    }

    public void ReplaceBuff(BuffAPI buff)
    {
        buffs.Remove(buff);
    }

}

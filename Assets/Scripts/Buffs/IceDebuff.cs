using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceDebuff : BuffAPI
{
    public float MoveSpeedPercentageDebuff;
    protected override void OnActivate(BuffReceiver target)
    {
        //Debuff Here
        target.GetComponent<PlayerController>().MoveSpeed *= MoveSpeedPercentageDebuff;
    }
    protected override void OnDeactivate(BuffReceiver target)
    {
        //RemoveDebuff Here
        target.GetComponent<PlayerController>().MoveSpeed /= MoveSpeedPercentageDebuff;
    }
}

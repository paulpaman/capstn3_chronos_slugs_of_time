using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPickup : Pickup
{
    protected override void ApplyEffect(Player player)
    {
        base.ApplyEffect(player);
        Lvl1Objectives.instance.AddBattery();
        Destroy(gameObject);
    }
}

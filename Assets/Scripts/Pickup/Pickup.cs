using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<AudioManager>().Play("itemPickup");
            Player player = collision.gameObject.GetComponent<Player>();
            if (player) ApplyEffect(player);
        }
    }

    protected virtual void ApplyEffect(Player player)
    {

    }
}

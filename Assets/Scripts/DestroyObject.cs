using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public void DestroyParent()
    {
        Destroy(this.gameObject.transform.parent);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField] string sceneToLoad;
 
    public void Load()
    {
        FindObjectOfType<ProgressSceneLoader>().LoadScene(sceneToLoad);
    }

    public void Reload()
    {
        Scene scene = SceneManager.GetActiveScene();

        //Time.timeScale = 1;

        FindObjectOfType<ProgressSceneLoader>().LoadScene(scene.name);
    }

}

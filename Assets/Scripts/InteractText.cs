using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InteractText : MonoBehaviour
{
    private static InteractText _instance;

    public static InteractText Instance { get { return _instance; } }

    public TextMeshProUGUI Text;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        Text.gameObject.SetActive(false);
    }

    public void ToggleText()
    {
        Text.gameObject.SetActive(!Text.gameObject.activeSelf);
    }


}

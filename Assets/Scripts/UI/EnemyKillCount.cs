using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemyKillCount : MonoBehaviour
{
    public TextMeshProUGUI killCount;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        killCount.text = " " + Lvl1Objectives.instance.enemiesKilled1;
    }
}

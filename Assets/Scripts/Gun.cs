using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public enum FireType { SingleFire,Automatic};
public enum BulletType { Single,Spread};
public class Gun : MonoBehaviour
{
    [SerializeField] FireType fireType;
    [SerializeField] BulletType bulletType;

    [Header("General Attributes")]
    public Animator GunAnim;
    public GameObject BulletImpactPrefab;
    int currentAmmo;
    int totalAmmo;
    public int MaxAmmo;
    public int ClipSize;
    public int WeaponDamage;
    public float FireRate;
    private float nextTimeToFire = 0f;
    public float recoilSpread;
    bool canReload = false;
    bool isReloading = false;
    bool shooting;
    private AudioSource audioSource;

    public bool isInfiniteAmmo = false;

    [Header("Spread Type Only")]
    public int BulletsPerShot;
 

    Camera playerCam;
    PlayerController player;

    [Header("Unlock")]
    public bool isUnlocked;


    void OnEnable()
    {
        //currentAmmo = ClipSize;
        UIManager.Instance.UpdateClipSizeText(currentAmmo);
        GunAnim.SetBool("isReloading", false);
        if(isInfiniteAmmo)
        {
            if (UIManager.Instance)
            {
                UIManager.Instance.InfiniteText.enabled = true;
                UIManager.Instance.AmmoText.enabled = false;
            }
        }
        else
        {
            if (UIManager.Instance)
            {
                UIManager.Instance.InfiniteText.enabled = false;
                UIManager.Instance.AmmoText.enabled = true;
            }
        }

        if (currentAmmo <= 0)
        {
            WeaponController.Instance.ReloadText.SetActive(true);
        }
        else
        {
            WeaponController.Instance.ReloadText.SetActive(false);
        }
    }

    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        if (player) playerCam = player.ViewCam;
        currentAmmo = ClipSize;
        if (currentAmmo <= 0)
        {
            WeaponController.Instance.ReloadText.SetActive(true);
        }
        else
        {
            WeaponController.Instance.ReloadText.SetActive(false);
        }
        UIManager.Instance.UpdateClipSizeText(currentAmmo);
        //totalAmmo = MaxAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.timeScale == 0)
        {
            return;
        }

        switch(fireType)
        {
            case FireType.SingleFire:
                shooting = Input.GetMouseButtonDown(0);
                break;
            case FireType.Automatic:
                shooting = Input.GetMouseButton(0);
                break;
            default:
                break;
        }


        if (shooting && Time.time >= nextTimeToFire)
        {
            if (currentAmmo > 0)
            {
                nextTimeToFire = Time.time + 1f / FireRate;
                //Shoot();
                GunAnim.SetTrigger("Shoot");
            }
            else
            {
                FindObjectOfType<AudioManager>().Play("dryFire");
                return;
            }
        }

        if(Input.GetKeyUp(KeyCode.R))
        {
            if(WeaponController.Instance)
            {
                if (currentAmmo < ClipSize)
                {
                    if (WeaponController.Instance.GetTotalAmmo() > 0)
                    {
                        FindObjectOfType<AudioManager>().Play("reload");
                        GunAnim.SetBool("isReloading", true);
                    }
                    else if (WeaponController.Instance.GetTotalAmmo() <= 0)
                    {
                        if (isInfiniteAmmo)
                        {
                            FindObjectOfType<AudioManager>().Play("reload");
                            GunAnim.SetBool("isReloading", true);
                        }
                    }
                }
            }
      
        }
    }

    public void Shoot()
    {
       switch(bulletType)
        {
            case BulletType.Single:
                SingleBullet();
                break;
            case BulletType.Spread:
                MultipleBullet();
                break;
        }
    }

    void SingleBullet()
    {
        float x = Random.Range(-recoilSpread, recoilSpread);
        float y = Random.Range(-recoilSpread, recoilSpread);

        Ray ray = playerCam.ViewportPointToRay(new Vector3(0.5f + x, 0.5f + y, 0f));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            Instantiate(BulletImpactPrefab, hit.point, transform.rotation);
            if (hit.transform.CompareTag("Enemy"))
            {
                Enemy enemy = hit.transform.GetComponent<Enemy>();
                BossHealthPart bossHealthPart = hit.transform.GetComponent<BossHealthPart>();

                if (enemy)
                {
                    enemy.TakeDamage(WeaponDamage);
                }

                if(bossHealthPart)
                {
                    bossHealthPart.TakeDamage(WeaponDamage);
                }

            }
            else if (hit.transform.CompareTag("BreakableWall"))
            {
                Debug.Log("Hit Breakable wall");
                BreakableWall breakableWall = hit.transform.GetComponent<BreakableWall>();
                if (breakableWall)
                {
                    breakableWall.TakeDamage(WeaponDamage);
                }
            }

            
            if (hit.transform.CompareTag("Crate"))
            {
                WeaponCrate weaponCrateScript = hit.transform.GetComponent<WeaponCrate>();
                if(weaponCrateScript)
                {
                    weaponCrateScript.TakeDamage(WeaponDamage);
                }
            }

        }
        else
        {

        }
        currentAmmo--;
        if (currentAmmo <= 0)
        {
            currentAmmo = 0;
            WeaponController.Instance.ReloadText.SetActive(true);
        }
        
        UIManager.Instance.UpdateClipSizeText(currentAmmo);
        //GunAnim.SetTrigger("Shoot");
    }

    void MultipleBullet()
    {
        for(int i = 0; i < BulletsPerShot; i++)
        {
            float x = Random.Range(-recoilSpread, recoilSpread);
            float y = Random.Range(-recoilSpread, recoilSpread);

            Ray ray = playerCam.ViewportPointToRay(new Vector3(0.5f + x, 0.5f + y, 0f));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Instantiate(BulletImpactPrefab, hit.point, transform.rotation);
                if (hit.transform.CompareTag("Enemy"))
                {
                    Enemy enemy = hit.transform.GetComponent<Enemy>();
                    BossHealthPart bossHealthPart = hit.transform.GetComponent<BossHealthPart>();

                    if (enemy)
                    {
                        enemy.TakeDamage(WeaponDamage);
                    }

                    if (bossHealthPart)
                    {
                        bossHealthPart.TakeDamage(WeaponDamage);
                    }
                }
                else if(hit.transform.CompareTag("BreakableWall"))
                {
                    Debug.Log("Hit Breakable wall");
                    BreakableWall breakableWall = hit.transform.GetComponent<BreakableWall>();
                    if (breakableWall)
                    {
                        breakableWall.TakeDamage(WeaponDamage);
                    }
                }

                if (hit.transform.CompareTag("Crate"))
                {
                    WeaponCrate weaponCrateScript = hit.transform.GetComponent<WeaponCrate>();
                    if(weaponCrateScript)
                    {
                        weaponCrateScript.TakeDamage(WeaponDamage);
                    }
                }
            }
            else
            {

            }
        }
        currentAmmo--;
        if (currentAmmo <= 0)
        {
            currentAmmo = 0;
            WeaponController.Instance.ReloadText.SetActive(true);
        }
        UIManager.Instance.UpdateClipSizeText(currentAmmo);
        //GunAnim.SetTrigger("Shoot");
    }

    public void Reload()
    {
        if(isInfiniteAmmo)
        {
            currentAmmo = ClipSize;
            UIManager.Instance.UpdateClipSizeText(currentAmmo);
        }
        else
        {
            if (WeaponController.Instance.TotalAmmo >= ClipSize - currentAmmo)
            {
                //GunAnim.SetTrigger("Reload");
                WeaponController.Instance.TotalAmmo -= (Mathf.Clamp(ClipSize - currentAmmo, 0, WeaponController.Instance.MaxAmmo));
                currentAmmo = ClipSize;

            }
            if (WeaponController.Instance.TotalAmmo < ClipSize - currentAmmo)
            {
                currentAmmo += WeaponController.Instance.TotalAmmo;
                WeaponController.Instance.TotalAmmo = 0;
            }

            UIManager.Instance.UpdateAmmoText(WeaponController.Instance.TotalAmmo);
            UIManager.Instance.UpdateClipSizeText(currentAmmo);
        }

        WeaponController.Instance.ReloadText.SetActive(false);
        GunAnim.SetBool("isReloading", false);
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}

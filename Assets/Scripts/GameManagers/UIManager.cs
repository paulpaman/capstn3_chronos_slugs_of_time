using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager Instance { get { return _instance; } }

    [Header("Health")]
    public TextMeshProUGUI HealthText;
    
    [Header("Armor")]
    public TextMeshProUGUI ArmorText;

    [Header("Ammo")]
    public TextMeshProUGUI AmmoText;
    public TextMeshProUGUI InfiniteText;

    [Header("Clip Size")]
    public TextMeshProUGUI clipSizeText;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public void UpdateHealthText(int num)
    {
        HealthText.text = num.ToString();
    }

    public void UpdateArmorText(int num)
    {
        ArmorText.text = num.ToString();
    }

    public void UpdateAmmoText(int num)
    {
        AmmoText.text = num.ToString();
    }

    public void UpdateClipSizeText(int num)
    {
        clipSizeText.text = num.ToString();
    }
}

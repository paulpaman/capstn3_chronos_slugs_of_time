﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ProgressSceneLoader : MonoBehaviour
{
    [SerializeField] Text progressText;
    [SerializeField] Slider slider;

    AsyncOperation operation;
    public Canvas canvas;


    private void Awake()
    {
        canvas = GetComponentInChildren<Canvas>(true);

        DontDestroyOnLoad(this.gameObject);
       
    }

    public void LoadScene(string sceneName)
    {
        UpdateProgressUI(0);
        canvas.gameObject.SetActive(true);
        StartCoroutine(BeginLoad(sceneName));
    }

    IEnumerator BeginLoad(string sceneName)
    {
        operation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);

        while(!operation.isDone)
        {
            UpdateProgressUI(operation.progress);
            yield return null;
        }

        UpdateProgressUI(operation.progress);
        operation = null;
        canvas.gameObject.SetActive(false);
    }

    public void UpdateProgressUI(float progress)
    {
        slider.value = progress;
        progressText.text = (int)(progress * 100f) + "%";
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickup : Pickup
{
    public int AmmoToAdd;

    protected override void ApplyEffect(Player player)
    {
        base.ApplyEffect(player);
        if (WeaponController.Instance)
        {
            WeaponController.Instance.AddTotalAmmo(AmmoToAdd);
        }
        Destroy(gameObject);
    }
}

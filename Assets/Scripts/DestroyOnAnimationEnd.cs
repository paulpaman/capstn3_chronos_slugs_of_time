using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnAnimationEnd : MonoBehaviour
{
    GameObject player;
    private void Start()
    {
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        transform.LookAt(player.transform, -Vector3.forward);
    }
    public void DestroyParent()
    {
        Destroy(gameObject.transform.parent.gameObject);
    }

    public void DestroyThis()
    {
        Destroy(this.gameObject);
    }

}

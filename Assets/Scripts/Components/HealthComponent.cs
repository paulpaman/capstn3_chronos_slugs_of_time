using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    private float currentHealth;
    public float MaxHealth;

    private void Awake()
    {
        currentHealth = MaxHealth;
    }
    private void Start()
    {
        
    }
    public void ReduceHealth(float damage)
    {
        currentHealth -= damage;
    }

    public void AddHealth(float num)
    {
        //Debug.Log("Heal Per Floor: " + num);
        currentHealth += num;
        if (currentHealth >= MaxHealth)
        {
            currentHealth = MaxHealth;
        }
    }

    public void SetMaxHealth(float num)
    {
        MaxHealth = num;
        currentHealth = MaxHealth;
    }

    public float GetCurrentHealth()
    {
        return currentHealth;
    }

}

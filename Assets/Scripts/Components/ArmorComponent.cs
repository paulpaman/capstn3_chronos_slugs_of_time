using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorComponent : MonoBehaviour
{
    private float currentArmor;
    public float MaxArmor;
    public float regenDuration;
    public float regenAmount;
    private float delay = 5f;
    Player player;
    [HideInInspector] public IEnumerator coroutine;
    [HideInInspector] public bool isRegen = false;
    private void Awake()
    {

    }
    private void Start()
    {
        player = GetComponent<Player>();
        currentArmor = MaxArmor;
    }
    public void ReduceArmor(float damage)
    {
        currentArmor -= damage;
    }

    public void AddArmor(float num)
    {
        //Debug.Log("Heal Per Floor: " + num);
        currentArmor += num;
        if (currentArmor >= MaxArmor)
        {
            currentArmor = MaxArmor;
        }
    }

    public float GetCurrentArmor()
    {
        return currentArmor;
    }

    IEnumerator RegenArmor()
    {
        isRegen = true;
        yield return new WaitForSeconds(delay);

        while (currentArmor < MaxArmor)
        {
            currentArmor += regenAmount;
            player.armorUI.fillAmount = currentArmor/MaxArmor;
            player.ScreenCrackUI.fillAmount = player.CalculateScreenCrack();
            yield return new WaitForSeconds(0.1f); 
        }
        isRegen = false;
    }

    public void StartArmorRegen()
    {
        coroutine = RegenArmor();
        StartCoroutine(coroutine);
    }

    public void StopArmorRegen()
    {
        StopCoroutine(coroutine);
        isRegen = !isRegen;
    }

    public void SetMaxArmor(float num)
    {
        MaxArmor = num;
        currentArmor = MaxArmor;
    }
}

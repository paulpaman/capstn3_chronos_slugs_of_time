using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Teleporter : MonoBehaviour
{
    public ObjectiveChecker objective3;

    public GameObject DeactivatedCube;
    public GameObject ActivatedCube;
    bool isActivated = false;
    bool canActivate = false;
    LoadScene loadScene;

    // Start is called before the first frame update
    void Start()
    {
        objective3 = GameObject.FindGameObjectWithTag("Teleporter").GetComponent<ObjectiveChecker>();
        loadScene = GameObject.Find("LoadScene").GetComponent<LoadScene>();
        DeactivatedCube.SetActive(true);
        ActivatedCube.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(canActivate)
        {
            if(Input.GetKeyDown(KeyCode.F))
            {
                if(isActivated)
                {
                    loadScene.Load();
                }
            }
        }
    }

    public void ActivateTeleporter()
    {
        if(Lvl1Objectives.instance)
        {
            if(Lvl1Objectives.instance.batteryCount >= 2 && Lvl1Objectives.instance.enemiesKilled1 >= 29)
            {
                canActivate = true;

                DeactivatedCube.SetActive(false);
                ActivatedCube.SetActive(true);

                ToggleText();

                objective3.StrikeThroughText();
                isActivated = true;
            }

            else
            {
                ToggleText();
            }
        }

        if(Level2Objectives.instance)
        {
            if(Level2Objectives.instance.enemiesKilled2 >= 37)
            {
                canActivate = true;
                DeactivatedCube.SetActive(false);
                ActivatedCube.SetActive(true);

                ToggleText();

                objective3.StrikeThroughText();
                isActivated = true;
            }

            else
                ToggleText();
        }

    }

    public void ToggleText()
    {
        // UI Popup
        if(canActivate)
        {
            if (InteractText.Instance)
            {
                InteractText.Instance.Text.text = "[F] Activate";
                InteractText.Instance.ToggleText();
            }
        }
        else
        {
            if (InteractText.Instance)
            {
                InteractText.Instance.Text.text = "Can't Activate";
                InteractText.Instance.ToggleText();
            }
        }
      
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TargetDetection : MonoBehaviour
{
    public UnityEvent OnPlayerHit;
    public UnityEvent OnPlayerExit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>())
        {
            OnPlayerHit.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>())
        {
            OnPlayerExit.Invoke();
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPGImpact : MonoBehaviour
{
    public int Damage;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Enemy"))
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            BossHealthPart bossHealthPart = other.gameObject.GetComponent<BossHealthPart>();
            if (enemy)
            {
                enemy.TakeDamage(Damage);
            }
            
            if (bossHealthPart)
            {
                bossHealthPart.TakeDamage(Damage);
            }
        }

        if (other.gameObject.CompareTag("BreakableWall"))
        {
            BreakableWall breakableWall = other.gameObject.GetComponent<BreakableWall>();
            if (breakableWall)
            {
                breakableWall.TakeDamage(Damage);
            }
        }

        //if (other.gameObject.CompareTag("Player"))
        //{
        //    Player player = other.gameObject.GetComponent<Player>();
        //    if (player) player.TakeDamage(Damage);
        //}
    }
}

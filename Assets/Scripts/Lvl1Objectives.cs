using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl1Objectives : MonoBehaviour
{
    private static Lvl1Objectives _instance;
    public static Lvl1Objectives instance {get {return _instance;} }
    public int batteryCount = 0;
    public int enemiesKilled1 = 0;
    public ObjectiveChecker objective1, objective2;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(enemiesKilled1 >= 29)
        {
            objective1.StrikeThroughText();
        }   

        if(batteryCount >= 2)
        {
            objective2.StrikeThroughText();
        }
    }

    public void AddBattery()
    {
        batteryCount++;
    }

    public void AddEnemiesKilled()
    {
        enemiesKilled1++;
    }
}

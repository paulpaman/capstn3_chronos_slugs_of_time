using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CrateUITracker : MonoBehaviour
{
    public TextMeshProUGUI crateDestroyedText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Level2Objectives.instance)
        {
            crateDestroyedText.text = "" + Level2Objectives.instance.destroyedCrates;
        }

        if(Level3Objectives.instance)
        {
            crateDestroyedText.text = "" + Level3Objectives.instance.destroyedCrates;
        }
    }
}

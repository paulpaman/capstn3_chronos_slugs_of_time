using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableWall : MonoBehaviour
{
    HealthComponent health;
    public GameObject NormalCube;
    public GameObject HitCube;
    void Start()
    {
        health = GetComponent<HealthComponent>();
        NormalCube.SetActive(true);
        HitCube.SetActive(false);
        //transform.Rotate(0, 90, 0);
        //transform.position = new Vector3(transform.position.x, transform.position.y, -0.5f);
    }

    public void TakeDamage(float damage)
    {
        health.ReduceHealth(damage);

        HitCube.SetActive(true);
        NormalCube.SetActive(false);
        

        if (health.GetCurrentHealth() <= 0)
        {
            Destroy(this.gameObject);
        }

        Invoke("ResetCube", 0.1f);
    }

    private void ResetCube()
    {
        NormalCube.SetActive(true);
        HitCube.SetActive(false);
    }
}

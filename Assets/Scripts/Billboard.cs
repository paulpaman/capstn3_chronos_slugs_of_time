using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    GameObject player;
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if(spriteRenderer)
        {
            spriteRenderer.flipX = true;
        }
      
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform, -Vector3.forward);
    }
}

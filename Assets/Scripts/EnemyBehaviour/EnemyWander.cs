using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyWander : StateMachineBehaviour
{
    public float wanderRadius;
    public float WanderTimer;
    private NavMeshAgent agent;
    float timer;
    Enemy enemyScript;
    Transform player;
    Transform self;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0f;
        enemyScript = animator.GetComponent<Enemy>();
        agent = enemyScript.agent;
        self = animator.transform;

        GameObject tempPlayer = GameObject.Find("Player");
        if (tempPlayer)
        {
            player = GameObject.Find("Player").transform;
        }

        //agent.speed /= 2f;
        Wander();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetFloat("AnimMoveX", CheckDirection());

        if (IsPlayerInRange(enemyScript.ShootingRange, player))
        {
            animator.SetTrigger("Shoot");
        }
        else
        {
            animator.SetBool("isMoving", false);
        }

        if (IsPlayerInRange(enemyScript.PlayerRange, player) || enemyScript.isHit == true)
        {
            animator.SetBool("isMoving", true);
        }
        else
        {
            animator.SetBool("isMoving", false);
        }

        timer += Time.deltaTime;
        if(timer >= WanderTimer)
        {
            animator.SetBool("isWandering", false);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0f;
        //agent.speed *= 2f;
        animator.SetBool("isWandering", false);
        enemyScript.StopAgent(true);
    }

    bool IsPlayerInRange(float range, Transform player)
    {
        //Debug.Log(Vector3.Distance(transform.position, player.transform.position));
        if (self)
        {
            if (player)
            {
                return Vector3.Distance(self.position, player.position) <= range;
            }

        }
        return false;
    }

    void Wander()
    {
        Vector3 newPos = RandomNavSphere(self.position, wanderRadius, -1);
        agent.SetDestination(newPos);
    }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    float CheckDirection()
    {
        float pos = enemyScript.GetOldPosX() - self.transform.position.x;

        return pos;
    }
}

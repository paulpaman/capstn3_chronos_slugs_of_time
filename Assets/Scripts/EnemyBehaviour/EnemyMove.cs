using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class EnemyMove : StateMachineBehaviour
{
    Enemy enemyScript;
    Transform player;
    Transform self;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyScript = animator.GetComponent<Enemy>();
        self = animator.transform;
        enemyScript.StopAgent(false);

        GameObject tempPlayer = GameObject.Find("Player");
        if (tempPlayer)
        {
            player = GameObject.Find("Player").transform;
        } 
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.SetFloat("AnimMoveX",CheckDirection());
        if(enemyScript.agent.isActiveAndEnabled == false)
        {
            return;
        }
        if (IsPlayerInRange(enemyScript.ShootingRange, player))
        {
            enemyScript.StopAgent(true);
            animator.SetTrigger("Attack");  
        }
        else
        {
            enemyScript.MoveTo(player);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("Attack");
        enemyScript.StopAgent(true);
    }

    bool IsPlayerInRange(float range, Transform player)
    {
        //Debug.Log(Vector3.Distance(transform.position, player.transform.position));
        if (self)
        {
            if (player)
            {
                return Vector3.Distance(self.position, player.position) <= range;
            }

        }
        return false;
    }

    float CheckDirection()
    {
        float pos = enemyScript.GetOldPosX() - self.transform.position.x;
       // pos = Mathf.Clamp(pos, -0.02f, 0.02f);
        //float dif = enemyScript.GetOldPosX() - self.transform.position.x;

        //Debug.Log(dif);
        //if (dif >= 0.1f)
        //{
        //    pos = 1f;
        //}
        //else if (dif <= -0.1f)
        //{
        //    pos = -1f;
        //}
        //else
        //{
        //    pos = 0f;
        //}

        return pos;
    }
}

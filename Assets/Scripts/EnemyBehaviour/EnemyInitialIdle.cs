using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInitialIdle : StateMachineBehaviour
{
    Enemy enemyScript;
    Transform player;
    Transform self;

    public float wanderTimer;
    private float timer;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0f;
        enemyScript = animator.GetComponent<Enemy>();
        self = animator.transform;
        GameObject tempPlayer = GameObject.Find("Player");
        if (tempPlayer)
        {
            player = GameObject.Find("Player").transform;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (IsPlayerInRange(enemyScript.ShootingRange, player))
        {
            animator.SetTrigger("Shoot");
        }
        else
        {
            animator.SetBool("isMoving", false);
        }

        if (IsPlayerInRange(enemyScript.PlayerRange, player) || enemyScript.isHit == true)
        {
            animator.SetBool("isMoving", true);
        }
        else
        {
            animator.SetBool("isMoving", false);
        }

        //Wander
        timer += Time.deltaTime;

        if (timer >= wanderTimer)
        {
            animator.SetBool("isWandering", true);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0f;
        //animator.ResetTrigger("Shoot");
    }

    bool IsPlayerInRange(float range, Transform player)
    {
        //Debug.Log(Vector3.Distance(transform.position, player.transform.position));
        if (self)
        {
            if (player)
            {
                return Vector3.Distance(self.position, player.position) <= range;
            }

        }
        return false;
    }
}

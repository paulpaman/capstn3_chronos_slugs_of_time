using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MacArthur_Idle : StateMachineBehaviour
{
    public float IdleTimer;
    float timer;

    int randomNumber;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0f;
        randomNumber = Random.Range(0, 2);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer += Time.deltaTime;
        if(timer >= IdleTimer)
        {
            if(randomNumber == 0)
            {
                animator.SetTrigger("Minigun");
            }
            else
            {
                animator.SetTrigger("Cannon");
            }
        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("Minigun");
        animator.ResetTrigger("Cannon");
    }
}

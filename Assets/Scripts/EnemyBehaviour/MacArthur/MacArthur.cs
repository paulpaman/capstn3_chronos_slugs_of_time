using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MacArthur : MonoBehaviour
{
    public GameObject CannonPrefab;
    public GameObject MinigunPrefab;

    public Transform MinigunFirePoint;
    public Transform CannonFirePoint;

    public Animator animator;
    public UnityEvent onDeath;

    void Start()
    {
       WeaponController.Instance.UnlockRPG();
    }

    void Update()
    {
        
    }

    public void SpawnMinigun()
    {
        EnemyBullet miniGun = Instantiate(MinigunPrefab, MinigunFirePoint.position, MinigunFirePoint.rotation).GetComponent<EnemyBullet>();
        miniGun.SetOwner(this.gameObject);
        FindObjectOfType<AudioManager>().Play("enemyShoot");
    }

    public void SpawnCannon()
    {
        EnemyBullet cannon = Instantiate(CannonPrefab, CannonFirePoint.position, CannonFirePoint.rotation).GetComponent<EnemyBullet>();
        cannon.SetOwner(this.gameObject);
        FindObjectOfType<AudioManager>().Play("bossCannon");
    }

    public void Die()
    {
        Destroy(gameObject.transform.parent.gameObject);
        onDeath.Invoke();
    }


}

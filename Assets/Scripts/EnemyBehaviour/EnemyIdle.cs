using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdle : StateMachineBehaviour
{
    Enemy enemyScript;
    Transform player;
    Transform self;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyScript = animator.GetComponent<Enemy>();
        self = animator.transform;
        GameObject tempPlayer = GameObject.Find("Player");
        if (tempPlayer)
        {
            player = GameObject.Find("Player").transform;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(IsPlayerInRange(enemyScript.ShootingRange,player))
        {
            animator.SetTrigger("Attack");
        }
        else
        {
            animator.SetBool("isMoving", false);
        }

        if (IsPlayerInRange(enemyScript.PlayerRange, player) || enemyScript.isHit == true)
        {
            animator.SetBool("isMoving", true);
        }
        else
        {
            animator.SetBool("isMoving", false);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("isMoving", true);
        animator.ResetTrigger("Attack");
    }

    bool IsPlayerInRange(float range, Transform player)
    {
        //Debug.Log(Vector3.Distance(transform.position, player.transform.position));
        if (self)
        {
            if (player)
            {
                return Vector3.Distance(self.position, player.position) <= range;
            }

        }
        return false;
    }

}

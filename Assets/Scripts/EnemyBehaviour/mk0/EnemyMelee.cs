using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMelee : StateMachineBehaviour
{
    Enemy enemyScript;
    Transform player;
    Transform self;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyScript = animator.GetComponent<Enemy>();
        enemyScript.StopAgent(true);
        //enemyScript.StartShooting();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyScript.isHit = false;
        //enemyScript.StopShooting();
        enemyScript.StopAgent(false);
    }

    bool IsPlayerInRange(float range, Transform player)
    {
        //Debug.Log(Vector3.Distance(transform.position, player.transform.position));
        if (self)
        {
            if (player)
            {
                return Vector3.Distance(self.position, player.position) <= range;
            }

        }
        return false;
    }
}

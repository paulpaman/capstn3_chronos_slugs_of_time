using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStatsManager : MonoBehaviour
{
    private static PlayerStatsManager _instance;
    public static PlayerStatsManager Instance { get { return _instance; } }

    public float CurrentMaxHealth;
    public float CurrentMaxArmor;
    public int CurrentPowerCells;
    Player player;

    public bool isSMGUnlocked;
    public bool isRPGUnlocked;
    public bool isMinigunUnlocked;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        SceneManager.sceneLoaded += OnSceneLoad;
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        Time.timeScale = 1.0f; 

        if(WeaponController.Instance)
        {
            CheckWeapons();
        }
    }

    public void SetPlayerStats()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        if (player)
        {
            player.ChangeMaxHealth(CurrentMaxHealth);
            player.ChangeMaxArmor(CurrentMaxArmor);
            player.SetCoin(CurrentPowerCells);
        }

    }

    public void AddPlayerMaxHealth(float num)
    {
        CurrentMaxHealth += num;
        player.ChangeMaxHealth(CurrentMaxHealth);
    }

    public void AddPlayerMaxArmor(float num)
    {
        CurrentMaxArmor += num;
        player.ChangeMaxArmor(CurrentMaxArmor);
    }

    public void AddPlayerPowerCells(int num)
    {
        CurrentPowerCells += num;
        player.AddCoin(num);
    }

    public void SubtractPlayerPowerCells(int num)
    {
        CurrentPowerCells -= num;
        player.SubtractCoin(num);
    }

    void CheckWeapons()
    {
        if(WeaponController.Instance)
        {
            if (isSMGUnlocked) WeaponController.Instance.UnlockSMG();
            if (isRPGUnlocked) WeaponController.Instance.UnlockRPG();
            if (isMinigunUnlocked) WeaponController.Instance.UnlockMinigun();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Transform DoorModel;
    public GameObject ColliderObject;

    public float OpenSpeed;

    bool shouldOpen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(shouldOpen && DoorModel.position.z !=1f)
        {
            DoorModel.position = Vector3.MoveTowards(DoorModel.position, new Vector3(DoorModel.position.x, DoorModel.position.y, 1f), OpenSpeed * Time.deltaTime);

            if(DoorModel.position.z == 1f)
            {
                ColliderObject.gameObject.SetActive(false);
            }
        }
        else if(!shouldOpen && DoorModel.position.z != 0f)
        {
            DoorModel.position = Vector3.MoveTowards(DoorModel.position, new Vector3(DoorModel.position.x, DoorModel.position.y, 0f), OpenSpeed * Time.deltaTime);

            if (DoorModel.position.z == 0f)
            {
                ColliderObject.gameObject.SetActive(true);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            shouldOpen = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            shouldOpen = false;
        }
    }
}

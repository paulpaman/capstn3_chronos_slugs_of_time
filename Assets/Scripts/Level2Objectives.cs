using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2Objectives : MonoBehaviour
{
    private static Level2Objectives _instance;
    public static Level2Objectives instance {get {return _instance;} }
    public int destroyedCrates = 0;
    public int enemiesKilled2 = 0;
    public ObjectiveChecker objective1, objective2;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(enemiesKilled2 >= 37)
        {
            objective1.StrikeThroughText();
        }   

        if(destroyedCrates >= 9)
        {
            objective2.StrikeThroughText();
            WeaponController.Instance.UnlockSMG();
        }
    }

    public void AddDestroyedCrates()
    {
        destroyedCrates++;
    }

    public void AddEnemiesKilled()
    {
        enemiesKilled2++;
    }
}

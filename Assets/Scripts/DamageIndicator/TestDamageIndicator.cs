using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDamageIndicator : MonoBehaviour
{
    [Range(5, 30)]
    [SerializeField] float destroyTimer = 15.0f;

    private void Start()
    {
        Invoke("Register",0f);
    }

    void Register()
    {
        if (!DI_System.CheckIfObjectInSight(this.transform))
        {
            DI_System.CreateIndicator(this.transform);

            Destroy(this.gameObject, destroyTimer);
        }
    }
}

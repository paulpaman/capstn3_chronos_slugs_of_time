using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameTrap : MonoBehaviour
{
    public Animator animator;
    public GameObject FlameDebuff;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            BuffReceiver buffReceiver = collision.GetComponent<BuffReceiver>();
            if (buffReceiver)
            {
                BuffAPI armorDebuff = Instantiate(FlameDebuff.GetComponent<BuffAPI>());
                buffReceiver.ApplyBuff(armorDebuff);
            }
        }
    }

    public void TriggerTrap()
    {
        animator.SetTrigger("Trap");
    }
}

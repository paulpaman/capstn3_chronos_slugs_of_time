using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthBase : MonoBehaviour
{
    float curHealth;
    float totalHealth;
    float damage;

    public GameObject MacArthur;
    public Image HealthBar;
    public List<BossHealthPart> HealthParts = new List<BossHealthPart>(); 
    private void Start()
    {
        foreach(BossHealthPart healthPart in HealthParts)
        {
            totalHealth += healthPart.GetHealth();
        }
        damage = totalHealth / HealthParts.Count;

        curHealth = totalHealth;
        HealthBar.fillAmount = curHealth / totalHealth;
    }

    public void TakeDamage()
    {
        curHealth -= damage;
        HealthBar.fillAmount = curHealth / totalHealth;
        if (curHealth <= 0)
        {
            MacArthur macArthur = MacArthur.GetComponent<MacArthur>();
            if(macArthur)
            {
                macArthur.animator.SetTrigger("Death");
            }

            //Complete Objective Here
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopUIManager : MonoBehaviour
{
    public GameObject Buttons;

    public void EnableButtons()
    {
        Buttons.SetActive(true);
    }

    public void DisableButtons()
    {
        Buttons.SetActive(false);
    }
}

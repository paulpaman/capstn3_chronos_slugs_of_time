using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    private static WeaponController _instance;

    public static WeaponController Instance { get { return _instance; } }

    public int MaxAmmo;
    [HideInInspector]public int TotalAmmo;

    public int selectedWeapon = 0;

    public List<GameObject> UnlockedWeaponList = new List<GameObject>();

    [Header("Weapons")]
    public Gun SMG;
    public Gun RPG;
    public Gun Minigun; 

    [Header("Unlock Panels")]
    public GameObject smgPanel;
    public GameObject minigunPanel;
    public GameObject rpgPanel;

    [Header("Additional")]
    public GameObject ReloadText;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        TotalAmmo = MaxAmmo;
        UIManager.Instance.UpdateAmmoText(TotalAmmo);
        CheckUnlockedWeapons();
        //SelectWeapon();
    }

    
    void Update()
    {
        if(Time.timeScale == 0)
        {
            return;
        }
        
        if(Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            //selectedWeapon = (selectedWeapon + 1) % transform.childCount;
            selectedWeapon = (selectedWeapon + 1) % UnlockedWeaponList.Count;
            SelectWeapon();
        }
        if(Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            //selectedWeapon = (selectedWeapon + (transform.childCount - 1)) % transform.childCount;
            selectedWeapon = (selectedWeapon + (UnlockedWeaponList.Count - 1)) % UnlockedWeaponList.Count;
            SelectWeapon();
        }

        NumberSelectWeapon();
    }

    void SelectWeapon()
    {
        int i = 0;
        foreach(GameObject weapon in UnlockedWeaponList)
        {
            if(i == selectedWeapon)
            {
                weapon.SetActive(true);
            }
            else
            {
                weapon.SetActive(false);
            }
            i++;
        }
    }

    void NumberSelectWeapon()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            ManualWeaponSelect(0);
        }
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            ManualWeaponSelect(1);
        }
        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            ManualWeaponSelect(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ManualWeaponSelect(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ManualWeaponSelect(4);
        }

    }

    void ManualWeaponSelect(int num)
    {
        if(num < UnlockedWeaponList.Count)
        {
            foreach (GameObject weapon in UnlockedWeaponList)
            {
                weapon.SetActive(false);
            }
            if (UnlockedWeaponList[num].GetComponent<Gun>().isUnlocked)
            {
                selectedWeapon = num;
                UnlockedWeaponList[num].SetActive(true);
            }
        }
       
    }

    void CheckUnlockedWeapons()
    {
        UnlockedWeaponList.Clear();
        foreach(Transform weapon in transform)
        {
            Gun gun = weapon.gameObject.GetComponent<Gun>();
            if(gun)
            {
                if(gun.isUnlocked)
                {
                    UnlockedWeaponList.Add(weapon.gameObject);
                }
            }
        }

        SelectWeapon();
    }

    public void AddTotalAmmo(int num)
    {
        TotalAmmo += num;
        if(TotalAmmo >= MaxAmmo)
        {
            TotalAmmo = MaxAmmo;
        }
        if (UIManager.Instance) UIManager.Instance.UpdateAmmoText(TotalAmmo);
    }

    public void ReplenishAmmo()
    {
        TotalAmmo = MaxAmmo;
        if (UIManager.Instance) UIManager.Instance.UpdateAmmoText(TotalAmmo);
    }

    public void UnlockSMG()
    {
        if (PlayerStatsManager.Instance) PlayerStatsManager.Instance.isSMGUnlocked = true;
        SMG.isUnlocked = true;
        CheckUnlockedWeapons();
        if(smgPanel) smgPanel.SetActive(true);
    }

    public void UnlockMinigun()
    {
        if (PlayerStatsManager.Instance) PlayerStatsManager.Instance.isMinigunUnlocked = true;
        Minigun.isUnlocked = true;
        CheckUnlockedWeapons();
        if(minigunPanel) minigunPanel.SetActive(true);
    }
    public void UnlockRPG()
    {
        if (PlayerStatsManager.Instance) PlayerStatsManager.Instance.isRPGUnlocked = true;
        RPG.isUnlocked = true;
        CheckUnlockedWeapons();
        if(rpgPanel) rpgPanel.SetActive(true);
    }

    public int GetTotalAmmo()
    {
        return TotalAmmo;
    }

}

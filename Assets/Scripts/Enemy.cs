using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.Events;
public class Enemy : MonoBehaviour
{
    public UnityEvent OnDeath;
    [HideInInspector] public NavMeshAgent agent;
    [SerializeField] Material hitMaterial;
    Material defaultMaterial;
    SpriteRenderer spriteRenderer;
    Player player;

    public float PlayerRange = 10f;
    public float ShootingRange = 3.5f;
    public bool CanShoot;
    public float FireRate;
    float shotCounter;
    public GameObject BulletPrefab;
    public Transform FirePoint;
    bool isShooting = false;
    bool isDead = false;
    float oldPos;
    public int Health;

    public int MeleeDamage;
    public int CoinToGive;

    public GameObject CoinTextPrefab;
    Animator animator;

    public bool isHit = false;

    [Header("For Green Enemy Only")]
    public Transform LeftFirePoint;
    public Transform RightFirePoint;
    //public GameObject RightBulletPrefab;
    //public GameObject LeftBulletPrefab;
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        NavMesh.avoidancePredictionTime = 4.0f;
        animator = GetComponent<Animator>();
        player = GameObject.Find("Player").GetComponent<Player>();

        spriteRenderer = GetComponent<SpriteRenderer>();
        defaultMaterial = spriteRenderer.material;
    }

    void Update()
    {

    }

    private void LateUpdate()
    {
        oldPos = transform.position.x;
    }

    public void MoveTo(Transform objToMoveTo)
    {
        agent.SetDestination(objToMoveTo.position);
    }

    public void TakeDamage(int damage)
    {
        isHit = true;
        Health -= damage;
        spriteRenderer.material = hitMaterial;

        if (Health <= 0f)
        {
            if(!isDead)
            {    
                isDead = true;

                if(PlayerStatsManager.Instance)
                {
                    //player.AddCoin(CoinToGive);
                    PlayerStatsManager.Instance.AddPlayerPowerCells(CoinToGive);
                }
                
                if(Level2Objectives.instance)
                {
                    Level2Objectives.instance.AddEnemiesKilled();
                }

                if(Lvl1Objectives.instance)
                {
                    Lvl1Objectives.instance.AddEnemiesKilled();
                }

                Invoke("ResetMaterial", 0.1f);

                animator.SetTrigger("Die");
                FindObjectOfType<AudioManager>().Play("warp");

                
                

                //Destroy(this.gameObject);
            }
        }
        else
        {
            Invoke("ResetMaterial", 0.1f);
        }
    }

    public void SpawnCoin()
    {
        GameObject coinText = Instantiate(CoinTextPrefab, this.transform);
        coinText.transform.parent = null;
        coinText.transform.GetChild(0).GetComponent<TextMeshPro>().SetText("+ " + CoinToGive.ToString() + " Power Cells");
        coinText.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

    public void Die()
    {
        Destroy(this.gameObject);
    }

    public void StopAgent(bool b)
    {
        if(agent)
        {
            agent.isStopped = b;
        }
        
    }

    public void Shoot()
    {
        FindObjectOfType<AudioManager>().Play("enemyShoot");
        EnemyBullet bullet = Instantiate(BulletPrefab, FirePoint.position, FirePoint.rotation).GetComponent<EnemyBullet>();
        bullet.SetOwner(this.gameObject);
    }

    public void LeftShoot()
    {
        EnemyBullet bullet = Instantiate(BulletPrefab, LeftFirePoint.position, LeftFirePoint.rotation).GetComponent<EnemyBullet>();
        bullet.SetOwner(this.gameObject);
    }

    public void RightShoot()
    {
        EnemyBullet bullet = Instantiate(BulletPrefab, RightFirePoint.position, RightFirePoint.rotation).GetComponent<EnemyBullet>();
        bullet.SetOwner(this.gameObject);
    }

    public void StartShooting()
    {
        isShooting = true;
    }

    public void StopShooting()
    {
        isShooting = false;
        CanShoot = true;
        shotCounter = FireRate;
    }

    public Vector3 GetNextPos()
    {
        return agent.nextPosition;
    }

    public float GetOldPosX()
    {
        return oldPos;
    }

    public void ResetMaterial()
    {
        spriteRenderer.material = defaultMaterial;
    }

    public void MeleeAttack()
    {
        FindObjectOfType<AudioManager>().Play("enemySlash");
        if(IsPlayerInRange(ShootingRange,player.transform))
        {
            player.TakeDamage(MeleeDamage);
        }
    }

    bool IsPlayerInRange(float range, Transform player)
    {

        if (player)
        {
            return Vector3.Distance(this.transform.position, player.position) <= range;
        }

        return false;
    }
}

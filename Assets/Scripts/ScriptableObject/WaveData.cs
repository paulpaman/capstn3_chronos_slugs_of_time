using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Wave Data", menuName = "WaveData")]
public class WaveData : ScriptableObject
{
    public Enemy[] EnemyList;
    public int TotalWave;

}
